# Introduction <a name="intro"></a>

A company's core competencies can have an impact on its IT infrastructure, IT architecture and IT security. These are technical issues. Core competencies need to be protected and further developed, but they also have an impact on the rules and regulations of corporate communications. Let's take a brief look at three different scenarios.


# 1 Compliance Regulations and Customer Data

Core expertise is critical for IT infrastructure because it ensures that the IT systems are aligned with the overall business objectives. For example, a financial services company may need to maintain strict compliance with regulations, which would impact the IT infrastructure's design and implementation. Additionally, core enterprise expertise is needed to ensure that the IT infrastructure is scalable and adaptable to changing business needs.

# 2 Manufacturing Data

Core enterprise expertise is also important for IT architecture because it ensures that the architecture is designed to support the organization's unique needs. For example, a manufacturing company may require real-time data analysis to optimize production, which would impact the IT architecture's design and implementation. Additionally, core enterprise expertise is needed to ensure that the IT architecture is secure and that data privacy and protection requirements are met.


# 3 IT Security

Core enterprise expertise is crucial for IT security because it ensures that the organization's IT systems are protected from threats and vulnerabilities. For example, a healthcare organization may need to maintain strict compliance with HIPAA regulations, which would impact the IT security measures implemented. Additionally, core enterprise expertise is needed to ensure that the IT security measures are up-to-date and that employees are trained to recognize and respond to security incidents.

# Knowhow Categories and communication rules
Clear guidelines for how IT security decisions are made and communicated to stakeholders, including how IT security aligns with the overall business strategy and objectives, as well as how security incidents are necessary.
The following table describes three level of knowledge.
 
| Core know-how      | Important know-how | Technical knowledge |
| ----------- | ----------- | ----------- |
| Knowledge of manufacturing steps that result in excellent quality; Knowledge about system interrelationships that enable new product groups      | Knowledge about project processes and decision contexts; Knowledge about basic manufacturing possibilities and technical interrelationships | Explicit knowledge, which can be researched and described |
| No digital documentation | in-house data processing | external data processing |



# Conclusion

In conclusion, core enterprise expertise plays a vital role in IT infrastructure, IT architecture, and IT security. Organizations must ensure that their IT systems are aligned with their business objectives and meet any regulatory requirements. Additionally, clear communication rules should be established to ensure that all stakeholders understand how IT decisions are made and how they align with the overall business strategy and objectives.


